/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package main;

import com.github.javafaker.Faker;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Ricard
 */
public class main {
    private static final Logger logger = LogManager.getLogger(main.class);
    
    public static void main(String[] args) {
        int numeroDeDataFalsa = 1000;
        List<Employee> lista = fakeData(numeroDeDataFalsa);
        
        try {
            crearRegistros(lista);
            lista = consultaListaEmpleados();
            for (Employee trabajador : lista) {
                Date fechaNacimiento = trabajador.getFechaNacimiento();
                Integer yearNumero = edatTrabajador(fechaNacimiento);
                logger.info(trabajador.toString()+", edad= "+yearNumero);
            }
            //Tambien se podria realizar la consulta con otro metodo propio y la query "count()" pero aprovecho que tengo la lista.
            logger.info("La DB y la tabla esta creada, el tamaño de 'empleados' es de: "+lista.size());
            pausaIntro();
            
            modificaRegistros();
            logger.info("La tabla ha sido alterada, pulse para mostrar:");
            pausaIntro();
            
            lista = consultaListaEmpleados();
            for (Employee trabajador : lista) {
                Date fechaNacimiento = trabajador.getFechaNacimiento();
                Integer yearNumero = edatTrabajador(fechaNacimiento);
                logger.info(trabajador.toString()+", edad= "+yearNumero);
            }
            logger.info("el tamaño de 'empleados' se ha reducido en: "+lista.size());
            
            logger.info("Deteniendo programa en 15 segundos");
            pausa(15000); //Pausa el programa para mantener la tabla con el contenido fake durante 15 segundos.
        } catch (ConstraintViolationException ex) {
            if (SingleSession.getInstance().getSessio().getTransaction() != null) {
                SingleSession.getInstance().getSessio().getTransaction().rollback();
            }
            logger.error("Violació de la restricció: " + ex.getMessage());
        } catch (HibernateException ex) {
            if (SingleSession.getInstance().getSessio().getTransaction() != null) {
                SingleSession.getInstance().getSessio().getTransaction().rollback();
            }
            logger.error("Excepció d'hibernate: " + ex.getMessage());
        } catch (Exception ex) {
            if (SingleSession.getInstance().getSessio().getTransaction() != null) {
                SingleSession.getInstance().getSessio().getTransaction().rollback();
            }
            logger.error("Excepció: " + ex.getMessage());
        } finally {
            SingleSession.getInstance().getSessio().close();
            SingleSession.getInstance().closeFactory();
            //Como he especificado en cfg create-drop las tablas se borran al acabar el programa
        }
    }
    
    public static List<Employee> consultaListaEmpleados(){
        Session session = SingleSession.getInstance().getSessio();
        List<Employee> lista = session.createNativeQuery("SELECT * FROM empleados",Employee.class).list();
        return lista;
    }
    
    public static void modificaRegistros(){
        Session session = SingleSession.getInstance().getSessio();
        List<Employee> lista = consultaListaEmpleados();
        Date fechaNacimiento;
        int yearNumero;
        Transaction transaccion = session.beginTransaction();
        for (Employee trabajador : lista) {
            fechaNacimiento = trabajador.getFechaNacimiento();
            yearNumero = edatTrabajador(fechaNacimiento);

            if (yearNumero >= 16 && yearNumero <= 18) {
                //Si l’empleat té entre 16 i 18 anys, afegeix la cadena “(junior)” al final del seu nom.
                trabajador.setNombre(trabajador.getNombre() + " (junior)");
                session.merge(trabajador);
            } else if (yearNumero > 50 && yearNumero < 66) {
                //Si l’empleat té més de 50 anys però menys de 66 anys, afegeix la cadena “(senior)” al final del seu nom.
                trabajador.setNombre(trabajador.getNombre() + " (senior)");
                session.merge(trabajador);
            } else if (yearNumero > 65) {
                //Si ‘empleat té més de 65 anys, elimina'l de la BBDD.
                session.remove(trabajador);
            }
        }
        transaccion.commit();
    }
    
    public static Integer edatTrabajador(Date fechaNacimiento){
        Integer yearNacimineto;
        LocalDate fechaNacimientoLocal = fechaNacimiento.toLocalDate();
        yearNacimineto = Period.between(fechaNacimientoLocal, LocalDate.now()).getYears();
        
        return yearNacimineto;
    }
    
    public static void pausa(int tiempoPausa){
        try {
            Thread.sleep(tiempoPausa);
        } catch (InterruptedException ex) {
            logger.error("Excepció Interrupció: " + ex.getMessage());
        }
    }
    
    public static void pausaIntro(){
        logger.info("Pausa - Pulse Intro para continuar con el programa...");
        new Scanner(System.in).nextLine();
    }
    
    public static void crearRegistros(List<Employee> lista) {
        Session session = SingleSession.getInstance().getSessio();
        Transaction transaccion = session.beginTransaction();
        for (Employee trabajador : lista) {
            session.persist(trabajador);
        }
        transaccion.commit();
    }
    
    public static List<Employee> fakeData(int numeroDeDataFalsa) {
        Faker faker = new Faker();
        List<Employee> lista = new ArrayList();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        String nombre;
        String apellidos;
        Date fechaNacimiento;
        int antiguitat;
        Integer edad;
        
        for (int i = 0; i < numeroDeDataFalsa; i++) {
            //Nombre
            nombre = faker.name().firstName();
            
            //Apellidos
            apellidos = faker.name().lastName();
            
            //Nacimiento. Importo util.Date para la fecha devuelta por faker
            java.util.Date fechaAleatoria = faker.date().birthday(16,75);
            fechaNacimiento = new java.sql.Date(fechaAleatoria.getTime());
            
            //Antiguitat
            int start = 3;
            int end = 20;
            antiguitat = faker.number().numberBetween(start, end);
            
            Employee trabajador = new Employee(nombre, apellidos, fechaNacimiento, antiguitat);
            lista.add(trabajador);
        }
        return lista;
    }
}
