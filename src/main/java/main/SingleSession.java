/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Ricard
 */
public class SingleSession {
    
    private static final String URLMYSQL = "jdbc:mysql://localhost:3306/empresa";
    private static final String USUARI = "root";
    private static final String CONTRASENYA = "Qf%V5rN2pA$Hr";
    
    private static SingleSession instancia;
    private SessionFactory factory;
    private Session s;
    
    private void InitSessionFactory() {
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        configuration.setProperty("hibernate.connection.username", USUARI);
        configuration.setProperty("hibernate.connection.password", CONTRASENYA);
        //Comprobar si la Database existe antes createDatabaseIfNotExist
        configuration.setProperty("hibernate.connection.url", URLMYSQL + "?createDatabaseIfNotExist=true");
        
        this.factory = configuration.buildSessionFactory();
        
        this.s = factory.openSession();
    }
    
    private SingleSession() {
        InitSessionFactory();
    }
    
    public static SingleSession getInstance() {
        if (instancia == null) {
            instancia = new SingleSession();
        }
        return instancia;
    }
    
    public Session getSessio() {
        return s;
    }
    
    public void closeFactory() {
        factory.close();
    }
}
